import java.io.File;
import java.util.Random;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;

public class prepRandomResponse extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			// create new message as a copy of the input
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);

			// get random file from directory
			String sRequest = inMessage.getRootElement().getFirstElementByPath("SOAP/Context/operation")
					.getValueAsString();
			String sMainDirPath = "/home/mqbrkr/able/";
			File[] files = new File(sMainDirPath + sRequest).listFiles();

			MbElement mbeEnv = outAssembly.getLocalEnvironment().getRootElement().getFirstChild();
			mbeEnv.createElementAsLastChild(MbElement.TYPE_NAME, "File", "");
			MbElement mbeFileElem = mbeEnv.getFirstElementByPath("File");

			if (files == null) {
				mbeFileElem.createElementAsLastChild(MbElement.TYPE_NAME, "Directory", sMainDirPath);
				mbeFileElem.createElementAsLastChild(MbElement.TYPE_NAME, "Name", "request_not_valid.xml");
			} else {
				File file = files[new Random().nextInt(files.length)];
				String sResponeFileName = file.getName();

				mbeFileElem.createElementAsLastChild(MbElement.TYPE_NAME, "Directory", sMainDirPath + sRequest);
				mbeFileElem.createElementAsLastChild(MbElement.TYPE_NAME, "Name", sResponeFileName);
			}

		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be
			// handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(), null);
		}
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(outAssembly);

	}
}
